# -*- coding: utf-8 -*-
"""Ensemble Learning.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1gVc3GffYNiJUHqnDTSOEFjAFBRQTT3pn
"""

#import data cleaning statements
import pandas as pd
import numpy as np
from scipy.io import loadmat
import matplotlib.pyplot as plt
import os

#Make values from the DASHLink Dataset
def pullValues(data, features, actual, standard):
  emptyList = []
  for i in range(len(features)):
    newArr = []
    arr = data[features[i]][0][0][0]
    for a in range(len(arr)):
      if ((a%(actual[i]/standard)) == 0):
        newArr.append(arr[a][0])
    emptyList.append(newArr)
  return pd.DataFrame(np.array(emptyList), features).T

def cutValues(data, size):
  for i in range(len(data["ALT"])):
    if (data["ALT"][len(data["ALT"])-1-i] >= 30+data["ALT"][len(data["ALT"])-1]):
      lowerLimit = len(data["ALT"])-i-(size-10)
      upperLimit = lowerLimit+size
      return (data.iloc[lowerLimit:upperLimit,:])

class AltitudeScaler():
  def __init__(self):
    self.groundAlt = 0
  def fit(self, data):
    self.groundAlt = data[len(data)-1]
  def transform(self, data):
    arr = []
    for i in range(len(data)):
      arr.append((data[i]-self.groundAlt))

    return arr
  def untransform(self, data):
    arr = []
    for i in range(len(arr)):
      arr.append(data[i] + groundAlt)
    return arr
  def fit_and_transform(self, data):
    self.fit(data)
    return self.transform(data)

class HeadingScaler():
  def __init__(self):
    self.runwayHeading = 0
  def fix(self, n):
    if (n <= 180.0) and (n >= -180.0):
      return n
    elif (n > 180.0):
      return self.fix(n-360.0)
    elif (n < -180.0):
      return self.fix(n+360.0)
  def fit(self, headingData, windData):
    self.runwayHeading = headingData[len(headingData)-1]
  def transform(self, headingData, windData):
    arr = []
    for i in range(len(headingData)):
      difference = -(self.runwayHeading - headingData[i])

      difference = self.fix(difference)

      arr.append(difference/180.0)
    arr2 = []
    for i in range(len(windData)):
      difference = -(self.runwayHeading - windData[i])
      difference = self.fix(difference)
      arr2.append(difference/180.0)
    return arr, arr2
  def untransform(self, headingData, windData):
    arr = []
    for i in range(len(headingData)):
      difference = headingData[i] * 180.0
      arr.append(difference+self.runwayHeading)
    arr2 = []
    for i in range(len(windData)):
      difference = windData[i] * 180.0
      arr2.append(difference+self.runwayHeading)
    return arr, arr2
  def fit_and_transform(self, headingData, windData):
    self.fit(headingData, windData)
    return self.transform(headingData, windData)

def validationChecks(data):
  if (np.max(data["ROLL"])-np.min(data["ROLL"]) < 12.0) and (np.max(data["RUDD"])-np.min(data["RUDD"]) < 15.0):
    return True
  else:
    return False

def differenceELEV(d1,d2):
  return (d1-(d2-85))

def differenceAil(d1,d2):
  return (d1-d2)

def div90(d1):
  return d1/90.0
def div100(d1):
  return d1/100.0

def div50(d1):
  return d1/50.0

def div350(d1):
  return d1/350.0

def div3000(d1):
  return d1/3000.0
setNum = input("Set number?")
flights = input("How many flights?")

for n in range(int(flights)):
  x = loadmat(f"{setNum}/File{n}.mat")
  y = pullValues(x, ["WD","WS","AIL_1","AIL_2","ELEV_1","ELEV_2", "RUDD","ALT", "GS", "TH","PTCH","ROLL", "PLA_1"], [4,4,1,1,1,1,2,4,4,4,8,8,4],1)
  John = y.copy()
    
  cValues = cutValues(John, 200)
  try:
    newAil = np.array(list(map(differenceAil, cValues["AIL_1"],cValues["AIL_2"])))

  except:
      print(f"{setNum}.{n}: Fail AIL")
      continue
  newElev = np.array(list(map(differenceELEV, cValues["ELEV_1"],cValues["ELEV_2"])))


  cValues["AIL"] = newAil
  cValues["ELEV"] = newElev

  if validationChecks(cValues):
    cValues["AIL"] = np.array(list(map(div90, cValues["AIL"])))
    cValues["ELEV"] = np.array(list(map(div90, cValues["ELEV"])))
    cValues["RUDD"] = np.array(list(map(div90, cValues["RUDD"])))
    cValues["WS"] = np.array(list(map(div50, cValues["WS"])))
    cValues["GS"] = np.array(list(map(div350, cValues["GS"])))

    altScaler = AltitudeScaler()
    altScaler.fit(np.array(cValues['ALT']))
    cValues["ALT"] = altScaler.transform(np.array(cValues["ALT"]))
    cValues["ALT"] = np.array(list(map(div3000, cValues["ALT"])))

    headingScaler = HeadingScaler()
    headingScaler.fit(np.array(cValues['TH']),np.array(cValues['WD']))
    cValues['TH'],cValues['WD'] = headingScaler.transform(np.array(cValues['TH']),np.array(cValues['WD']))
    
    cValues.to_csv(f"Set{setNum}/{n}.csv")
    print(f"{setNum}.{n}: Success")
  else:
    print(f"{setNum}.{n}: Fail")
    continue
